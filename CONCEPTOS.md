# Trabajo práctico 2
## Markdown y herramientas de programación colaborativas

### Git
#### Comprensión
1. Un *software de control de versiones* es un sistema que lleva resgistro de los cambios de un conjunto de archivos, permitiendo volver a versiones específicas en el futuro si así se requiere.
2. Un *repositorio* es un lugar donde se almacena y organiza información de un proyecto. El carácter de público o privado depende del acceso que el público tenga al mismo; si es accesible al público o si su acceso es restringido.
3. Un *árbol de Merkle* es una estrutura de datos que resume una gran cantidad de datos en único bloque de datos verificable, para lograrlo usa varias capas de bloques (cada uno identificado por un hash) que se relacionan unívocamente con un bloque superior (root), entonces si cambia la información de algún bloque camibia la información del root. Git utiliza esta estructura para el seguimientos de cambios del repositorio porque cada bloque que datos nuevo genera un hash único, llamado commit, lo que permite ir de un commit a otro para ver el estado del proyecto en ese momento sin afectar el estado actual.
4. Una copia remota de datos es la que se encuentra almacenada en servidores remotos a diferencia de una copia local que se almacena en el equipo.
5. Como se mencionó en la pregunta 3, un *commit* es un estado del proyecto en cierto momento. En git, el comando commit se usa para confirmar los cambios realizados y enviarlos al directorio git (o HEAD).
6. En caso de que los avances de un proyecto deban ser modificados y sea necesario volver a una versión anterior, se debe ejecutar el comando `git reset --hard origin/master`, el cuál anula todos los cambios locales y commits, y vuelve a la última versión del servidor.
7. Una rama (o branch) es una bifurcación del proyecto, como si fuera una linea alterna de tiempo, que permite desarrollar funcionalidades, reparar errores y bugs, sin modificar la versión estable (branch master).

### Markdown
1. _Markdown_ es un lenguaje de marcado que se usa para dar formato a los textos mediante convenciones sencillas en el uso de caracteres y texto plano.
2. Inicialmente markdown fue creado para que facilite la conversión de texto plano en HTML válido (lenguaje de marcado utilizado en desarrollo de páginas de internet); pero su fácilidad y practicidad hacen que sea útil para cualquier tipo de texto.





