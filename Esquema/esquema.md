# Esquema UMl de las ramas en git

<script src="mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

```mermaid
sequenceDiagram
Participant Master
Participant tema1
Participant esquema
note left of Master: Rama principal
Master->> tema1:Nueva rama
tema1-->> tema1 : carpeta tema1 y\n tema1.md añadidos
tema1 -->> Master: checkout Master
Master -->> tema1: checkout tema1 
tema1-->> tema1 : cambios en tema1.md
tema1->> Master: Merge tema1
Master->> esquema: Nueva rama
esquema -->> esquema: diagrama uml
esquema-->> esquema: esquema.md modificado
esquema ->> Master: Merge esquema
```
