# Manejo de ramas con git

- Con el comando `git checkout master` pase de la rama tema1 a la rama principal master y la carpeta que habia creado "tema1" no estaba.

- Al hacer `git checkout tema1` volví a la rama tema1 y la carpeta volvió a aparecer.

