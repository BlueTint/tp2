# Trabajo Prático 2
## Probando Markdown desde gitlab WebIde

### Encabezados
Se hacen poniendo # al comienzo de la linea. Hay 6 niveles, de mayor a menor tamaño.
Para los niveles 1 y 2 también se pueden hacer poniendo en la linea de abajo ==== o ----- respectivamente.

Ejemplo nivel 1
=====
### Párrafos
Para la separación de párrafos se deja una línea blanca en el medio.

Para la separación de línea (continuar en la línea de abajo) se dejan 2 espacios al  
final de la línea.

### Comandos y códigos
Para que md no interprete el lenguaje, se pone al texto entre dos ´´.  
`Es importante que sean `` iguales `

### **Negrita**, _cursiva_ y sus mezclas
Para negrita hay que poner el texto con `**dos asteriscos por lado**`, __también__ pueden ser con dos __ (guión bajo)  
Para cursiva es lo mismo pero con solo un *carácter* por lado.  
Para las combinaciones se usan **_tres caracteres_** por lado y si se sigue el orden se pueden usar * y _.

### <s>Linea al medio<s/>
Se hace escribiendo el texto entre `<s> y <s/>`

### Subíndice y Superíndice
Para hacer un subíndice se escribe `<sub>subíndice<sub/>` al lado de la palabra que quiero  
ALGO<sub>subíndice<sub/>  
Para hacer un superíndice es lo mismo pero escribiendo sup en vez de sub.  
ALGO<sup>superíndice<sup/>

### Listas
- Para hacer una
- lista de puntos
- solo hay que poner un signo menos 
- al comienzo de la línea 
- y dar un espacio

+ También funciona con signos mas .

* y con asteriscos

1. Para una lista numerada
2. hay que poner
3. número. y un espacio
53. Este tipo de lista sigue un orden: por mas que escriba 53. aparece en el prewiew como 4.
La lista se corta al escribir  y dejar un la línea blanca
- Para una lista 
    - con sub listas
        1. Se usa tab 
        2. Por cada sub lista
            - y así sucesivamente
        3. Se pueden usar tanto números como caracteres para que sean puntos

### Enlaces
Para poner un link se pone el nombre del link entre corchetes y al lado, entre paréntesis se pone la url `[link](url)`  
[ejemplo](https://gitlab.com)  
Otra forma de hacerlo, para que aparezca el lin tal cual, es escribiendo la url signos de menor (<) y mayor (>).  
<https://gitlab.com>

### Lineas horizontales
- - -
Se hacen escribiendo *** o - - - y escribiendo debajo
*** 

### Citas
Para hacer una cita
> Se debe escribir > al comienzo de la linea  
> Y  también cada vez que se continue  
> en las lineas de abajo

### Imagenes
Para poner una imagen se escribe `![](url de la imagen)`
(los corchetes se dejan vacios)

### Tablas 
Las tablas se hacen separando las columnas con barras verticales | (alt+124 en windows)  
Para que la primer fila sea la principal se pone ---|--- por cada colummna.  
(nota: se pueden poner más --- para que sea estético, pero con -- ya funciona)   
Se puede elegir la orientación del texto, para izquierda es :---, para central es :---: y para derecha es ---:  

columna 1 | columna 2 | columna 3
--|--|--
ejemplo 1 | texto 2 | etc
para | mas | filas
no hace |falta separar | con --/--

Para separar entre tabla y tabla, se puede dejar una línea blanca.

### Graficos
Usando [mermaid live editor](https://mermaid-js.github.io/mermaid-live-editor/) puedo hacer distintos graficos, y después copiar y pegar el codigo markdown


[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcblx0QVtQcm95ZWN0byBkZSBnaXRdIC0tPiBDWzMgc2VjY2lvbmVzIF1cblx0QyAtLT58V29ya2luZyBEaXJlY3Rvcnl8IERbY29waWEgZGUgdW5hIHZlcnNpw7NuIGRlbCBwcm95ZWN0b11cblx0QyAtLT58U3RhZ2luZyBBcmVhfCBFW8OhcmVhIGRlIHByZXBhcmFjacOzbiwgaW5mbyBxdWUgdmEgZW4gbGEgY29uZmlybWFjacOzbl1cblx0QyAtLT58LmdpdCBEaXJlY3Rvcnl8IEZbcmVwb3NpdG9yaW8sIGJhc2UgZGUgZGF0b3MgZGVsIHByb3llY3RvIF1cblx0XHRcdFx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcblx0QVtQcm95ZWN0byBkZSBnaXRdIC0tPiBDWzMgc2VjY2lvbmVzIF1cblx0QyAtLT58V29ya2luZyBEaXJlY3Rvcnl8IERbY29waWEgZGUgdW5hIHZlcnNpw7NuIGRlbCBwcm95ZWN0b11cblx0QyAtLT58U3RhZ2luZyBBcmVhfCBFW8OhcmVhIGRlIHByZXBhcmFjacOzbiwgaW5mbyBxdWUgdmEgZW4gbGEgY29uZmlybWFjacOzbl1cblx0QyAtLT58LmdpdCBEaXJlY3Rvcnl8IEZbcmVwb3NpdG9yaW8sIGJhc2UgZGUgZGF0b3MgZGVsIHByb3llY3RvIF1cblx0XHRcdFx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

Otra forma usando código?

<script src="mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

```mermaid
graph TD
	A[proyecto git] --> C{tres estados}
	C -->|working directory| D[copia de una versión del proyecto]
	C -->|staging area| E[area de preparación]
	C -->|git directory| F[lo que se copia cuando clonamos un repositorio]
					

```

<script src="mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

```mermaid

	pie title Pets adopted by volunteers
	"Dogs" : 386
	"Cats" : 85
	"Rats" : 15	
```












