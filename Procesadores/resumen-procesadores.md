# Organización básica de una computadora simple
## Resumen de la unidad 2 del libro Organización de computadoras de A.Tanenbaum (4ed)


Una computadora digital simple consiste en un sistema de procesadores interconectados, memorias y dispositivos de I/O. Todos los componentes están conectados mediante **buses** (alambres paralelos que transmiten direcciones,datos y señales de control), que pueden ser externos o internos a la CPU.
La unidad central de procesamiento o **CPU** (Central Processing Unit) es el componente que ejecuta los programas almacenados en la memoria principal: busca sus instrucciones, las examina y las ejecuta una por una.  

La CPU tiene varias partes:
- **_Unidad de control_** → Busca instrucciones de la memoria principal y determina su tipo.  
- **_ALU_** → Unidad de aritmética y lógica, hace operaciones sencillas como sumas y operaciones de álgebra booleana (AND,OR,NOT); para ejecutar las instrucciones.
- **_Registros_** → Se almacenan resultados temporales y cierta información de control (en una memoria pequeña y de muy rápido acceso. Todos tienen el mismo tamaño y contienen 1 número por registro (menor que cierto máximo determinado). Los mas importantes son el *contador de programa* (**PC**), que indica la siguiente instruccion que debe buscar para ejecutar; y el *registro de instrucciones* (**IR**) que contiene la instrucción que se está ejecutando.

Una parte de la CPU, llamada **camino de datos** se encarga de la relación entre la ALU y los registros: Se eligen ciertos registros y se llevan a los registros de entrada de la ALU; una vez que entran, la ALU opera con ellos y el resultado es un registro de salida que se envía al sector de registros y si se quiere se puede guardar en la memoria.  
**Ciclo del camino de datos** → hace pasar dos operandos por la ALU y almacena el resultado. Entre más rápido es el ciclo, más rápido funciona la máquina.  

<script src="mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

```mermaid

	graph LR
	A{Intrucciones} --> C(Registro-Memoria)
	A --> B(Registro-Registro)
	C-->|Busca| D( Palabras de la memoria a los registros, que le sirven a la ALU como entradas para las instrucciones que siguen.)
	C--> |Permite| G( Almacenar el contenido de un registro en la memoria.)
	B--> |Busca| F(Operandos de los registros, los coloca en los registros de entrada de la ALU, opera con ellos y coloca el resultado en uno de los registro.)	
```

